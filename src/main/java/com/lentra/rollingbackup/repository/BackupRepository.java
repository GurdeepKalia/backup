package com.lentra.rollingbackup.repository;

import com.lentra.rollingbackup.RollingBackupApplication;
import com.lentra.rollingbackup.service.FileService;
import com.lentra.rollingbackup.utils.Constants;
import com.lentra.rollingbackup.utils.Util;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class BackupRepository {

    Logger logger = LoggerFactory.getLogger(BackupRepository.class);

    public void backupCollection(MongoTemplate mongoTemplate, String collection, int batch){
        try {
            FileService.addToFile("Collection : " +collection);
            String backupCollection = getBackupCollectionName(collection);
            int rowNumber = 0;
            final long totalRows = mongoTemplate.getCollection(collection).countDocuments();
            logger.info("Collection : " +collection  +" | Total documents : " +totalRows);
            if(!mongoTemplate.collectionExists(backupCollection)) {
                do {
                    List<Document> documents = new ArrayList<>();
                    FindIterable<Document> findIterable = mongoTemplate.getCollection(collection).find()
                            .sort(new BasicDBObject("$natural", 1)).skip(rowNumber).limit(batch);
                    for (Document obj : findIterable) {
                        documents.add(obj);
                    }
                    mongoTemplate.getCollection(backupCollection).insertMany(documents);
                    rowNumber = rowNumber + documents.size();
                } while (rowNumber < totalRows);
            }

            FileService.addToFile("Backup status : Success\n" );
            logger.info("Collection : " + collection + " -------->Backup done!");
        }catch (Exception e){
            FileService.addToFile("Backup status : Fail\n" );
            mongoTemplate.dropCollection(getBackupCollectionName(collection));
            RollingBackupApplication.errorOccured = true;
            logger.error(e.getMessage(),e);
        }
    }

    public void removeBackupCollectionBeforeRetention(MongoTemplate mongoTemplate,String backupCollectionName){
        mongoTemplate.dropCollection(backupCollectionName);
    }
    private String getBackupCollectionName(String originalCollectionName){
        return "bkp_" +Util.getSimpleDateFormatter().format(new Date()) +"_" +originalCollectionName;
    }
}
