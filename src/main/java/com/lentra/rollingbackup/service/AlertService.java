package com.lentra.rollingbackup.service;

import org.springframework.stereotype.Service;

public interface AlertService {
    public void sendEmail(boolean errorOccured,boolean executionCompleted, String emailListFile, String smtpHost, String smtpPort);
}
