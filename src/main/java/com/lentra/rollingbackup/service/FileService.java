package com.lentra.rollingbackup.service;

import com.lentra.rollingbackup.utils.Constants;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

@Service
public class FileService {

    private static final FileWriter fileWriter;
    private static final PrintWriter printWriter;
    static {
        try {
            Files.deleteIfExists(new File(Constants.attachmentFilePath).toPath());
            Files.createFile(new File(Constants.attachmentFilePath).toPath());
            fileWriter = new FileWriter(Constants.attachmentFilePath);
            printWriter = new PrintWriter(fileWriter);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void addToFile(String text){
        printWriter.println(text);
    }

    public static void closeResources(){
        try {
            printWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
