package com.lentra.rollingbackup.service;

import com.lentra.rollingbackup.RollingBackupApplication;
import com.lentra.rollingbackup.model.Database;
import com.lentra.rollingbackup.repository.BackupRepository;
import com.lentra.rollingbackup.utils.Util;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BackupService {

    @Autowired
    private BackupRepository backupRepository;

    Logger logger = LoggerFactory.getLogger(BackupService.class);

    public void backup(Database database){
        try(MongoClient mongoClient = MongoClients.create(database.getUri())) {
            for (Database.Schema schema : database.getSchemas()) {
                FileService.addToFile("Schema : " +schema.getSchemaName() +"");
                FileService.addToFile("------------------------------------\n");
                logger.info("Starting backup for schema : " +schema.getSchemaName());
                backupSchema(mongoClient, schema,database.getBatch());
                logger.info("Starting backup for schema : " +schema.getSchemaName() +" ---------->DONE");
                logger.info("Retention days for database " +database.getName() +" : " +database.getRetentionDays());
                applyRetention(mongoClient,schema,database.getRetentionDays());
            }
            
        }
    }

    private void backupSchema(MongoClient mongoClient, Database.Schema schema, int batch){
        try {
            MongoTemplate mongoTemplate = getMongoTemplate(mongoClient, schema);
            schema.getCollections().forEach(collection -> backupRepository.backupCollection(mongoTemplate, collection, batch));
        }catch(Exception e){
            RollingBackupApplication.errorOccured = true;
            logger.error(e.getMessage(),e);
        }
    }

    public void applyRetention(MongoClient mongoClient, Database.Schema schema,String retentionDays){
        try {
            logger.info("Starting retention for : " + schema.getSchemaName());
            MongoTemplate mongoTemplate = getMongoTemplate(mongoClient, schema);
            Date retentionDate = DateUtils.addDays(new Date(), Integer.parseInt("-" + retentionDays));
            Set<String> backupsToRemove = mongoTemplate.getCollectionNames().stream().filter(backupCollectionName -> backupCollectionName.startsWith("bkp_")).filter(backupCollectionName -> canDeleteBackupCollection(backupCollectionName, retentionDate)).collect(Collectors.toSet());
            backupsToRemove.forEach(backupToRemove -> backupRepository.removeBackupCollectionBeforeRetention(mongoTemplate, backupToRemove));
            FileService.addToFile("\nSchema Retention status : Success\n\n");
            logger.info("Starting retention for : " + schema.getSchemaName() + " --------->DONE");
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            FileService.addToFile("\nSchema Retention status : Failed\n\n");
            RollingBackupApplication.errorOccured = true;
        }
    }

    private boolean canDeleteBackupCollection(String backupCollectionName,Date retentionDate){
        return getDateFromBackupCollectionName(backupCollectionName).before(retentionDate);
    }

    private Date getDateFromBackupCollectionName(String backupCollectionName){
        String dateFromCollectionName = backupCollectionName.substring(4,14);
        Date date = new Date();
        try {
             date = Util.getSimpleDateFormatter().parse(dateFromCollectionName);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private MongoTemplate getMongoTemplate(MongoClient mongoClient, Database.Schema schema) {
        return new MongoTemplate(mongoClient, schema.getSchemaName());
    }
}
