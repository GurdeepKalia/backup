package com.lentra.rollingbackup.service;

import com.lentra.rollingbackup.RollingBackupApplication;
import com.lentra.rollingbackup.utils.Constants;
import email.EmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class AlertServiceImpl implements AlertService{

    Logger logger = LoggerFactory.getLogger(AlertServiceImpl.class);

    @Autowired
    Environment environment;

    @Override
    public void sendEmail(boolean errorOccured,boolean executionCompleted, String emailListFile, String smtpHost, String smtpPort) {
        String activeProfile = environment.getActiveProfiles()[0];
        String[] args ;
        if(activeProfile.equals("uat")){
            logger.info("Sending alert mail for uat");
            if(errorOccured || !executionCompleted) {
                args = new String[]{new File(Constants.attachmentFilePath).getAbsolutePath(),emailListFile,smtpHost,smtpPort, "{UAT} " +Constants.errorSubject,Constants.errorBody};
                EmailSender.main(args);
            }else{
                args = new String[]{new File(Constants.attachmentFilePath).getAbsolutePath(),emailListFile,smtpHost,smtpPort, "{UAT} " +Constants.successSubject,Constants.successBody};
                EmailSender.main(args);
            }
            logger.info("Sending alert mail for uat---------->DONE");
        }
        else if(activeProfile.equals("prod")){
            logger.info("Sending alert mail for Prod");
            if(errorOccured || !executionCompleted) {
                args = new String[]{new File(Constants.attachmentFilePath).getAbsolutePath(),emailListFile,smtpHost,smtpPort, "{PROD} " +Constants.errorSubject,Constants.errorBody};
                EmailSender.main(args);
            }else{
                args = new String[]{new File(Constants.attachmentFilePath).getAbsolutePath(),emailListFile,smtpHost,smtpPort, "{PROD} " +Constants.successSubject,Constants.successBody};
                EmailSender.main(args);
            }
            logger.info("Sending alert mail for Prod---------->DONE");
        }
        else{
            args = new String[]{new File(Constants.attachmentFilePath).getAbsolutePath(),emailListFile,"gurdeepkalia4@gmail.com","<password>", "test" , "{DEV} " +Constants.errorSubject,Constants.errorBody};
            //EmailSender.main(args);
            logger.info("Development env. So, alert mail skipped.");
        }
    }
}
