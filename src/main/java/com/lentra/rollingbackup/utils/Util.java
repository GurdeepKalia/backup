package com.lentra.rollingbackup.utils;

import java.text.SimpleDateFormat;

public class Util {

    private static final String dateFormat = "dd-MM-yyyy";
    public static SimpleDateFormat getSimpleDateFormatter(){
        return new SimpleDateFormat(dateFormat);
    }
}
