package com.lentra.rollingbackup.utils;

public class Constants {
    public static final String successSubject = "Backup process completed successfully.";
    public static final String successBody = "Backup successful. Please check attachment for backup information.";

    public static final String errorSubject = "Backup process completed with some errors.";
    public static final String errorBody = "Backup completed partially. Please check attachment for backup information. Also check the loggers to investigate the errors occurred during backup process";

    public static final String attachmentFilePath = "backup_status.txt";
    public static final String emailListFilePath = "src/main/resources/email.txt";

}
