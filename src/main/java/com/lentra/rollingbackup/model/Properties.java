package com.lentra.rollingbackup.model;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@ConfigurationProperties("mongodb")
public class Properties {
    private ArrayList<Database> dbDetails;

    public ArrayList<Database> getDbDetails() {
        return dbDetails;
    }

    public void setDbDetails(ArrayList<Database> dbDetails) {
        this.dbDetails = dbDetails;
    }

    @Override
    public String toString() {
        return "Properties{" +
                "dbDetails=" + dbDetails +
                '}';
    }
}
