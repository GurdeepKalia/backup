package com.lentra.rollingbackup.model;

import java.util.ArrayList;

public class Database {
    private String name;
    private String uri;
    private String retentionDays;
    private ArrayList<Schema> schemas;
    private int batch;

    public static class Schema{
        private String schemaName;
        private ArrayList<String> collections;

        public String getSchemaName() {
            return schemaName;
        }

        public ArrayList<String> getCollections() {
            return collections;
        }

        public void setSchemaName(String schemaName) {
            this.schemaName = schemaName;
        }

        public void setCollections(ArrayList<String> collections) {
            this.collections = collections;
        }

        @Override
        public String toString() {
            return "Schema{" +
                    "schemaName='" + schemaName + '\'' +
                    ", collections=" + collections +
                    '}';
        }
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public String getRetentionDays() {
        return retentionDays;
    }

    public ArrayList<Schema> getSchemas() {
        return schemas;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setRetentionDays(String retentionDays) {
        this.retentionDays = retentionDays;
    }

    public void setSchemas(ArrayList<Schema> schemas) {
        this.schemas = schemas;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    @Override
    public String toString() {
        return "Database{" +
                "name='" + name + '\'' +
                ", uri='" + uri + '\'' +
                ", retentionDays='" + retentionDays + '\'' +
                ", schemas=" + schemas +
                ", batch=" + batch +
                '}';
    }
}
