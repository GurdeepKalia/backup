package com.lentra.rollingbackup;

import com.lentra.rollingbackup.model.Database;
import com.lentra.rollingbackup.model.Properties;
import com.lentra.rollingbackup.service.AlertService;
import com.lentra.rollingbackup.service.BackupService;
import com.lentra.rollingbackup.service.FileService;
import com.lentra.rollingbackup.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@SpringBootApplication
public class RollingBackupApplication implements CommandLineRunner {

	@Autowired
	private Properties properties;

	@Autowired
	private BackupService backupService;

	@Autowired
	private AlertService alertService;

	private String smtpHost;
	private String smtpPort;

	public static boolean errorOccured = false;
	public static boolean executionCompleted = false;

	Logger logger = LoggerFactory.getLogger(RollingBackupApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RollingBackupApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if(args.length < 3){
			logger.error("Please specify arguments in the order <email-list> <smtp-host> <port> . Make sure to pass first argument as the absolute path to email list file");
			System.exit(0);
		}
		Path emailListFile = Paths.get(args[0]);
		if (!emailListFile.toFile().exists()) {
			logger.error("Email list file does not exist " + emailListFile);
			System.exit(0);
		}

		smtpHost = args[1];
		smtpPort = args[2];

		try{
			ArrayList<Database> databases = properties.getDbDetails();
			for(Database database : databases){
				FileService.addToFile("======================================================");
				FileService.addToFile("Database : " +database.getName());
				FileService.addToFile("======================================================\n");
				logger.info("*****************Database : " +database.getName() + "*****************");
				backupService.backup(database);
			}
			logger.info("Backup process completed!");
			executionCompleted = true;
		}catch(Exception e){
			logger.error(e.getMessage(),e.fillInStackTrace());
			errorOccured = true;
		}
		finally {
			FileService.closeResources();
			logger.info("Sending alert mail.");
			alertService.sendEmail(errorOccured,executionCompleted,emailListFile.toString(),smtpHost,smtpPort);
			logger.info("Sending alert mail----------->DONE");
			Files.deleteIfExists(new File(Constants.attachmentFilePath).toPath());
		}
	}
}
